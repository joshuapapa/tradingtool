# This trading tool is an original idea. 

One day, i realized that the 90% philippine market volume is cause by 30 brokers.  
This tool provide insights on broker action and which stocks are active. This is also add TA and FA.  

The data is manually extracted on the broker which only takes 10 mins of my time. Without this tool, the checking and analysis of price of all stocks will take a day.  

The future of this tool is to be converted on desktop/web app and the long term is to teach an AI to buy, hold and sell using reinforcement learning.  

To use it, pick the tab first 'Blue Chip' or 'Non Blue Chip' tab then sort by Sum of Variance

A few of my analysis is here: https://www.investagrams.com/Profile/jdpapa
